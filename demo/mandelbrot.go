package main

import (
	"image"
	"image/color"
)

func Mandelbrot(x, y, w, h int) int {
	it := 0
	x0 := realStart + (float64(x)/float64(w))*(realEnd-realStart)
	y0 := imaginaryStart + (float64(y)/float64(h))*(imaginaryEnd-imaginaryStart)
	fx := 0.
	fy := 0.

	for ; fx*fx+fy*fy <= 4 && it < maxIteration; it++ {
		fxt := fx*fx - fy*fy + x0
		fy = 2.*fx*fy + y0
		fx = fxt
	}

	if it >= maxIteration {
		return -1
	}
	return it
}

var (
	from = color.RGBA{
		R: 0,
		G: 0,
		B: 32,
		A: 255,
	}
	to = color.RGBA{
		R: 255,
		G: 0,
		B: 0,
		A: 255,
	}
)

func DrawPixel(ret *image.RGBA, x int, y int, it int) {
	if it >= 0 {
		var computed color.RGBA

		computed.R = uint8(int32(from.R) + int32(float64(int16(to.R) - int16(from.R)) / 255. * float64(it)))
		computed.G = uint8(int32(from.G) + int32(float64(int16(to.G) - int16(from.G)) / 255. * float64(it)))
		computed.B = uint8(int32(from.B) + int32(float64(int16(to.B) - int16(from.B)) / 255. * float64(it)))
		computed.A = 255

		ret.Set(x, y, computed)
	} else {
		ret.Set(x, y, color.RGBA{
			R: 0,
			G: 0,
			B: 0,
			A: 255,
		})
	}
}
