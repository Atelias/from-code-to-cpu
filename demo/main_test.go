package main

import "testing"

func BenchmarkComputeRowMultitask(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ComputeRowMultitask(1024, 1024)
	}
}

func BenchmarkComputeMultiWorkers(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ComputeMultiWorkers(1024, 1024)
	}
}

func BenchmarkComputeMultiWorkersColumns(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ComputeMultiWorkersColumns(1024, 1024)
	}
}