package main

import (
	"fmt"
	"image"
	"image/png"
	"os"
	"runtime"
	"runtime/trace"
	"sync"
)

const (
	filename       = "./mandelbrot.png"
	width          = 1500
	height         = 1000
	maxIteration   = 255
	realStart      = -0.747968852903576
	realEnd        = -0.698970286289854
	imaginaryStart = -0.229563420709648
	imaginaryEnd   = -0.266312345669939
)

func main() {
	trac, _ := os.Create("workerBuffered.trace")
	_ = trace.Start(trac)

	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	img := computeWorkersBuffered(width, height)

	if err := png.Encode(f, img); err != nil {
		panic(err)
	}
	fmt.Println("Image created", f)
	trace.Stop()
}

func computeSequential(w, h int) image.Image {
	ret := image.NewRGBA(image.Rect(0, 0, w, h))

	for i := 0; i < w; i++ {
		for j := 0; j < h; j++ {
			it := Mandelbrot(i, j, w, h)
			DrawPixel(ret, i, j, it)
		}
	}
	return ret
}

func computeParallel(w, h int) image.Image {
	ret := image.NewRGBA(image.Rect(0, 0, w, h))

	wg := sync.WaitGroup{}
	wg.Add(w)
	for i := 0; i < w; i++ {
		go func() {
			for j := 0; j < h; j++ {
				it := Mandelbrot(i, j, w, h)
				DrawPixel(ret, i, j, it)
			}
			wg.Done()
		}()
	}
	wg.Wait()
	return ret
}

func computeFullParallel(w, h int) image.Image {
	ret := image.NewRGBA(image.Rect(0, 0, w, h))

	wg := sync.WaitGroup{}
	wg.Add(w * h)
	for i := 0; i < w; i++ {
		for j := 0; j < h; j++ {
			go func() {
				it := Mandelbrot(i, j, w, h)
				DrawPixel(ret, i, j, it)
			}()
			wg.Done()
		}
	}
	wg.Wait()
	return ret
}

func computeWorkers(w, h int) image.Image {
	ret := image.NewRGBA(image.Rect(0, 0, w, h))
	c := make(chan image.Point, 1)

	wg := sync.WaitGroup{}
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			for p := range c {
				it := Mandelbrot(p.X, p.Y, w, h)
				DrawPixel(ret, p.X, p.Y, it)
			}
			wg.Done()
		}()
	}

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			c <- image.Point{j, i}
		}
	}
	close(c)
	wg.Wait()
	return ret
}

func computeWorkersBuffered(w, h int) image.Image {
	ret := image.NewRGBA(image.Rect(0, 0, w, h))
	c := make(chan image.Point, w*h)

	wg := sync.WaitGroup{}
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			for p := range c {
				it := Mandelbrot(p.X, p.Y, w, h)
				DrawPixel(ret, p.X, p.Y, it)
			}
			wg.Done()
		}()
	}

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			c <- image.Point{j, i}
		}
	}
	close(c)
	wg.Wait()
	return ret
}

